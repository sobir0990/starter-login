<?php
namespace app\widgets;

use app\models\Notification;
use app\models\Project;
use app\models\TenderBankPayment;
use app\models\User;
use Yii;
use yii\base\Widget;
use yii\db\Expression;

/**
 * Class NotificationWidget
 * @package app\widgets
 */
class NotificationWidget extends Widget
{
    private $_list;

    private $_count;

    /**
     * @inheritDoc
     */
    public function init()
    {

        $query = Notification::find()
            ->where([
                'to_user_id'=>Yii::$app->user->id,
                'status'=>Notification::STATUS_NEW
            ]);

        $this->_count = $query->count();

        $this->_list = $this->_count?$query->limit(5)->all():null;

        parent::init();
    }

    /**
     * @return string|void
     */
    public function run()
    {
        return $this->render('notification',[
            'models'=>$this->_list,
            'count'=>$this->_count,
        ]);
    }

}
















