<?php

use app\models\Project;
use app\models\ProjectTaskComment;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $models ProjectTaskComment[] | Project[] | \app\models\TenderBankPayment[]*/
/** @var $count integer */
/** @var $type string */

?>

<li class="">
    <div class="dropdown">
        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
            <i class="fas fa-bell"></i>
            <?= $count?'<span class="badge badge-danger">'.$count.'</span>':'' ?>
        </a>
        <div class="dropdown-menu dropdown-menu-right notification">
            <?php if($count){ ?>
                <div class="noti-head">
                    <h6 class="d-inline-block m-b-0"><?= t('Notifications') ?></h6>
                </div>
                <ul class="noti-body">
                    <?php
                    $url = $all_url = $name = $text = null;
                    foreach ($models as $model) {
                        if($type=='task-comment'){
                            $url = Url::to(['estemete/view', 'id'=>$model->project_task_id, '#'=>'report-'.$model->id]);
                            $all_url = ['notification/index'];
                            $name = $model->projectTask->name;
                            $text = $model->statusAction.
                                '<span class="n-time text-muted">
                                     <i class="icon feather icon-clock mr-1"></i>'.
                                     Yii::$app->formatter->asDatetime($model->created_at).
                                '</span>';
                        }elseif ($type=='project' && User::isAdmin()){

                            if($model instanceof Project) {

                                $url = $model->status == $model::STATUS_TENDER_WINNER ?
                                    Url::to(['project/add-tender-data', 'id' => $model->id,]) :
                                    Url::to(['project/view', 'id' => $model->id,]);

                                $all_url = ['project/index'];
                                $name = $model->name;

                                $date = $model->updated_at;
                                $status = $model->statusLabel;
                                if ($model->status == $model::STATUS_DESIGNING_COMPLETE) {
                                    $date = $model->projector_end_time;
                                } elseif ($model->status == $model::STATUS_TENDER_WINNER) {
                                    $date = $model->commission_time;
                                } elseif ($model->status == $model::STATUS_PROGRESS) {
                                    $status = '<span class="badge p-1 badge-success">' . t('All task completed') . '</span>';
                                }
                            }else{
                                $url = Url::to(['payment/tender-bank-payment', 'id' => $model->id]);
                                $all_url = ['payment/tender-bank-payment'];
                                $name = t('Bank tender payment');
                                $date = $model->created_at;
                                $status = '<span class="badge p-1 badge-danger">'.t("Waiting confirm").'</span>';
                            }

                            $text = str_replace('<br>',' ',$status).
                                '<span class="n-time text-muted">
                                     <i class="icon feather icon-clock mr-1"></i>'.
                                Yii::$app->formatter->asDatetime($date).
                                '</span>';
                        }elseif ($type=='project' && User::isCommission()){
                            $url = Url::to(['tender/index', 'id'=>$model->id,]);
                            $all_url = ['tender/list'];
                            $name = $model->name;
                            $date = $model->tender_date;

                            $text = str_replace('<br>',' ',$model->statusLabel).
                                '<span class="n-time text-muted">
                                     <i class="icon feather icon-clock mr-1"></i>'.
                                Yii::$app->formatter->asDate($date).
                                '</span>';
                        }elseif ($type=='customer'){
                            $url = Url::to(['order/view', 'id'=>$model->id,'#'=>'estimate-section']);
                            $all_url = ['order/index'];
                            $name = $model->name;
                            $date = $model->projector_end_time;

                            $text = str_replace('<br>',' ',$model->statusLabel).
                                '<span class="n-time text-muted">
                                     <i class="icon feather icon-clock mr-1"></i>'.
                                Yii::$app->formatter->asDate($date).
                                '</span>';
                        }elseif ($type=='projector'){
                            $url = Url::to(['designing/view', 'id'=>$model->id]);
                            $all_url = ['designing/index'];
                            $name = $model->name;
                            $date = $model->updated_at;

                            $text = str_replace('<br>',' ',$model->statusLabel).
                                '<span class="n-time text-muted">
                                     <i class="icon feather icon-clock mr-1"></i>'.
                                Yii::$app->formatter->asDate($date).
                                '</span>';
                        }

                        ?>
                        <li class="notification p-0">
                            <a class="w-100 d-inline-block p-3" href="<?= $url ?>">
                                <div class="media">
                                    <div class="media-body">
                                        <p>
                                            <strong class="text-overflow"><?= Html::encode((string)$name) ?></strong>
                                        </p>
                                        <p>
                                            <?= $text ?>
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                <div class="noti-footer">
                    <?= Html::a(t('Show all'),$all_url) ?>
                </div>
            <?php }else{ ?>
                <div class="noti-footer">
                    <p class="text-danger m-2"><?= t('Notification not found') ?></p>
                </div>
            <?php } ?>
        </div>
    </div>
</li>
