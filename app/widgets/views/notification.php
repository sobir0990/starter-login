<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $models \app\models\Notification[] */
/** @var $count integer */

?>

<li class="">
    <div class="dropdown">
        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
            <i class="fas fa-bell"></i>
            <?= $count?'<span class="badge badge-danger">'.$count.'</span>':'' ?>
        </a>
        <div class="dropdown-menu dropdown-menu-right notification">
            <?php if($count){ ?>
                <div class="noti-head">
                    <h6 class="d-inline-block m-b-0"><?= t('Notifications') ?></h6>
                </div>
                <ul class="noti-body">
                    <?php foreach ($models as $model) {
                        $text = Html::encode($model->text);
                        ?>
                        <li class="notification p-0">
                            <a class="w-100 d-inline-block p-3" href="<?= Url::to(['site/notification','id'=>$model->id]) ?>">
                                <div class="media">
                                    <div class="media-body">
                                        <p data-toggle="tooltip" title="<?=$text?>">
                                            <strong class="text-overflow-two-line mb-2 l-h-1-3"><?= $text ?></strong>
                                            <span class="n-time text-muted">
                                                <i class="icon feather icon-clock mr-1"></i>
                                                <?= Yii::$app->formatter->asDatetime($model->created_at)  ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                <div class="noti-footer">
                    <?= Html::a(t('Show all'),['site/notifications']) ?>
                </div>
            <?php }else{ ?>
                <div class="noti-footer">
                    <p class="text-danger m-2"><?= t('Notification not found') ?></p>
                </div>
            <?php } ?>
        </div>
    </div>
</li>
