<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 11/14/18
 * Time: 4:25 PM
 */

namespace app\widgets;

use yii\helpers\Url;

/**
 * Class Breadcrumbs
 * @package app\widgets
 */
class Breadcrumbs extends \yii\bootstrap4\Breadcrumbs
{
    public $encodeLabels = false;

    public $homeLink =  [
        'label' => '<i class="fa fa-home"></i>',
        'url'=>'/',
    ];
}




















