<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 11/21/18
 * Time: 9:49 AM
 */
namespace app\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class LightGallery
 * @package app\assets
 */
class LightGallery extends AssetBundle
{
    public $sourcePath = '@webroot/libs/lightgallery';

    public $css = [
        'lightgallery.css',
    ];
    public $js = [
        "picturefill.min.js",
        "jquery.mousewheel.min.js",
        "lightgallery-all.min.js",
    ];

    public $depends = [
        JqueryAsset::class
    ];
}
