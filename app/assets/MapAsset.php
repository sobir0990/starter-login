<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class MapAsset
 * @package app\assets
 */
class MapAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        "map/leaflet.css",
        "map/leaflet-routing-machine.css",
        "map/Control.Geocoder.min.css"
    ];

    public $js = [
        "map/leaflet.js",
        "map/routing_machine.js",
        "map/Control.Geocoder.min.js",
        "map/markercluster.js"
    ];

}
