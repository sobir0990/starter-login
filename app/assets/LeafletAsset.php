<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 11/21/18
 * Time: 9:49 AM
 */
namespace app\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class LeafletAsset
 * @package app\assets
 */
class LeafletAsset extends AssetBundle
{
    public $sourcePath = '@webroot/libs/leaflet';

    public $css = [
        'leaflet.css',
        'leaflet.fullscreen.css',
        'leaflet-gesture-handling.min.css'
    ];
    public $js = [
        "leaflet.js",
        "leaflet-routing-machine.js",
        "Leaflet.fullscreen.min.js",
        'leaflet-gesture-handling.min.js',
        'leaflet.markercluster-src.js'
    ];

    public $depends = [
        JqueryAsset::class
    ];
}
