<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 11/21/18
 * Time: 9:49 AM
 */
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class HighChartsAsset
 * @package app\assets
 */
class HighChartsAsset extends AssetBundle
{
    public $sourcePath = '@webroot/libs/highcharts';
    
    public $js = [
        "highcharts.js",
        "series-label.js",
        "exporting.js",
        "export-data.js",
        "accessibility.js",
        "highcharts-more.js",
    ];

}
