<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class MapAsset
 * @package app\assets
 */
class FrontMapAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        "front_map/leaflet.css",
        "front_map/leaflet-routing-machine.css",
        "front_map/MarkerCluster.css",
        "front_map/MarkerCluster.Default.css",
        "map/Control.Geocoder.min.css"
    ];

    public $js = [
        "front_map/leaflet.js",
        "front_map/route_machine.js",
        "front_map/MarkerCluster.js",
        "map/Control.Geocoder.min.js",
    ];

}
