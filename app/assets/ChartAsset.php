<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class ChartAsset
 * @package app\assets
 */
class ChartAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [

    ];

    public $js = [
      "libs/chart/animated.js",
      "libs/chart/index.js",
      "libs/chart/percent.js",
      "libs/chart/xy.js",
    ];

}
