<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 11/21/18
 * Time: 9:49 AM
 */
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class HighMapAsset
 * @package app\assets
 */
class HighMapAsset extends AssetBundle
{
    public $sourcePath = '@webroot/libs/highmap';
    
    public $js = [
        "highmaps.js",
        "map.js",
        "uz-all.js",
    ];
}
