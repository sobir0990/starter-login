<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 11/21/18
 * Time: 9:49 AM
 */
namespace app\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class DataTableAsset
 * @package app\assets
 */
class DataTableAsset extends AssetBundle
{
    public $sourcePath = '@webroot/libs/datatables';

    public $css = [
        'datatables.min.css'
    ];

    public $js = [
        "datatables.min.js",
    ];

    public $depends = [
        JqueryAsset::class
    ];

}
