<?php

namespace app\controllers;

use app\components\Controller;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Response;
use Yii;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        $this->layout = 'login';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new User(['scenario' => User::SCENARIO_LOGIN]);
        if ($model->runValidate() && $model->login()) {
            return $this->goHome();
        }

        $model->password = '';
        return $this->render("login", [
            "model" => $model
        ]);
    }

    public function actionRegister()
    {
        $this->layout = 'login';

        $model = new User();
        $model->scenario = User::SCENARIO_CREATE;
        $model->status = User::STATUS_ACTIVE;

        if ($model->runValidate()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->setPassword($model->password);
                $model->confirm_password = $model->password;
                $model->save();
                Html::alertSuccess(t("Успешно сохранено!"));
                $transaction->commit();
                return $this->goHome();
            } catch (Exception $e) {
                Html::alertTransactionException($e);
                $transaction->rollback();
            }
        }


        return $this->render('register', [
            'model' => $model
        ]);
    }

    public function actionSendMail()
    {
        $this->layout = 'login';

        $model = new User();
        $model->scenario = User::SCENARIO_EDIT;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::find()->where(["email" => $model->email])->one();
            if ($user) {
                $this->sendMessage($user);
                Html::alertSuccess(t("Ссылка для сброса пароля была отправлена вашу электронную почту"));
                return $this->redirect('/login');
            } else {
                $model->addError("email", "Электронная почта не найдена");
            }
        }

        return $this->render('send-mail', [
            'model' => $model
        ]);
    }

    public function sendMessage($user)
    {
        $mailer = Yii::$app->mailer;
        $host = 'http://starter.loc';
        $message = $mailer->compose()
            ->setFrom(["from@gmail.com" => 'Your Name'])
            ->setTo($user->email)
            ->setSubject('Subject')
            ->setTextBody('Plain text content')
            ->setHtmlBody('<a href="' . $host . Url::to(['/reset-password', 'hash' => $user->password]) . '">изменение парол</a>')
            ->send();

        return $message;
    }

    public function actionResetPassword($hash)
    {
        $model = User::find()->andWhere(['password' => $hash])->one();

        if (!$model) {
            return false;
        }

        $model->scenario = User::SCENARIO_CHANGE_PASSWORD;

        $model->password = '';

        if ($model->runValidate()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->setPassword($model->password);
                $model->confirm_password = $model->password;
                $model->save();
                Html::alertSuccess(t("Успешно сохранено!"));
                $transaction->commit();
                return $this->goHome();
            } catch (Exception $e) {
                Html::alertTransactionException($e);
                $transaction->rollback();
            }
        }

        return $this->render('reset-password', [
            'model' => $model
        ]);
    }

    /**
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();

    }


}
