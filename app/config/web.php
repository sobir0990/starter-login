<?php

use kartik\grid\Module as GridModule;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\bootstrap4\BootstrapAsset as Bootstrap4Asset;
use yii\web\JqueryAsset;
use app\components\User as WebUser;
use app\models\User;
use yii2tech\authlog\AuthLogWebUserBehavior;
use app\components\ApplicationInitBehavior;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'starter',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'Asia/Tashkent',
    'language' => 'ru',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'gridview' => [
            'class' => GridModule::class,
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'rqpgvHsmPbrWCuIpRaaC2_v2qFGHgiII',
        ],
        'formatter' => [
            'defaultTimeZone' => 'Asia/Tashkent',
            'dateFormat' => 'dd.MM.yyyy',
            'datetimeFormat' => 'dd.MM.yyyy HH:mm',
            'decimalSeparator' => '.',
            'thousandSeparator' => ' ',
            'currencyCode' => 'RUS',
            'timeFormat' => 'HH:mm',
            'sizeFormatBase' => '1024.0001',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => WebUser::class,
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => array('/site/login'),
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'your_host',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'your_mail@gmail.com',
                'password' => 'your_password',
                'port' => '587', // Port 25 is a very common port too
                'encryption' => 'tls', // It is often used, check your provider or mail server specs
            ],
        ],
        'assetManager' => [
            'linkAssets' => true,
//            'appendTimestamp' => true,
            'bundles' => [
                BootstrapAsset::class => [
                    'css' => [],
                ],
                Bootstrap4Asset::class => [
                    'css' => []
                ],
                BootstrapPluginAsset::class => [
                    'js' => []
                ],
                \yii\bootstrap4\BootstrapPluginAsset::class => [
                    'js' => []
                ],
                JqueryAsset::class => [
                    'js' => [],
                    'depends' => [
                        \app\assets\AppAsset::class
                    ]
                ],
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => \codemix\localeurls\UrlManager::class,
            'languages' => array_keys(LANGUAGES),
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableDefaultLanguageUrlCode' => false,
            'enableLanguageDetection' => false,
            'rules' => [
                '/' => 'site/index',
                'send-mail' => 'site/send-mail',
                'restore-password' => 'site/restore-password',
                'login' => 'site/login',
                'register' => 'site/register',
                'reset-password' => 'site/reset-password',
                'logout' => 'site/logout',
                "<controller>" => "<controller>/index",
                "<controller>/<action>/<id>" => "<controller>/<action>",
                "<controller>/<action>" => "<controller>/<action>",
            ],
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '195.158.1.181'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
