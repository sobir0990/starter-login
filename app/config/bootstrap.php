<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 6/29/19
 * Time: 4:10 PM
 */


require_once __DIR__ . '/' . "defines.php";

Yii::$classMap['yii\helpers\Html'] = "@app/components/Html.php";
Yii::$classMap['yii\helpers\ArrayHelper'] = "@app/components/ArrayHelper.php";
Yii::$classMap['yii\helpers\StringHelper'] = "@app/components/StringHelper.php";
Yii::$classMap['kartik\widgets\Select2'] = "@app/components/Select2.php";
Yii::$classMap[\lavrentiev\widgets\toastr\NotificationFlash::class] = dirname(__DIR__)."/components/NotificationFlash.php";
Yii::$classMap[\lavrentiev\widgets\toastr\NotificationBase::class] = dirname(__DIR__)."/components/NotificationBase.php";
Yii::$classMap[\lavrentiev\widgets\toastr\ToastrAsset::class] = dirname(__DIR__)."/assets/ToastrAsset.php";

/**
 * O'zgaruvchini print_r/var_dump qilib chiqarish.
 * Bu funksiya buferni avtomatik tozalaydi.
 *
 * @param $variable O'zgaruvchi
 * @param bool $pre <pre> teginishi chiqarish yoki chiqarmaslik
 * @param bool $dump - print_r yoki var_dump
 */
function print_variable($variable, $pre = true, $dump = false)
{
    while (ob_get_level()) {
        ob_get_clean();
    }

    if ($pre)
        echo "<pre>";

    if ($dump)
        var_dump($variable);
    else
        print_r($variable);

    echo "</pre>";
    exit();
}

/**
 * Yii:t funksiyasining soddalashtirilgan varianti.
 *
 * @see Yii::t()
 *
 * @param $text
 * @param array $params
 * @param null $lang
 * @return string
 */
function t($text, $params = [], $lang = null)
{
    return Yii::t("yii", $text, $params, $lang);
}

function __($text, $params = [], $lang = null)
{
    return Yii::t("yii", $text, $params, $lang);
}

