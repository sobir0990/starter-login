<?php

namespace app\models;

use app\components\ActiveRecord;
use app\components\HtmlTagValidator;
use app\components\interfaces\StatusInterface;
use app\components\traits\StatusTrait;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $login
 * @property string|null $name
 * @property string $email
 * @property int|null $phone
 * @property int|null $status
 * @property string $password
 * @property string $created_date
 * @property int|null $city_id
 * @property int|null $avatar_id
 * @property string|null $self_about
 * @property array|null $account_details
 *
 * @property-read bool $isActive
 */
class User extends ActiveRecord implements IdentityInterface, StatusInterface
{

    use StatusTrait {
        getStatuses as traitGetStatuses;
    }

    const SCENARIO_CREATE = 'create';
    const SCENARIO_LOGIN = "login";
    const SCENARIO_EDIT = 'edit';
    const SCENARIO_CHANGE_PASSWORD = 'change-password';


    /** @var self */
    private $_user;

    /** Loginda email  */
    public $rememberMe = false;

    public $new_password, $current_password, $confirm_password;

    public $loginByToken = false;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_date',
                ],
                'value' => function ($event) {
                    return date('Y-m-d');
                },
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [["email", "password"], "required",
                "on" => [self::SCENARIO_LOGIN]],

            [["login", "email", "password", "confirm_password"], "required",
                "on" => [self::SCENARIO_CREATE]],

            [["password", "confirm_password"], "required",
                "on" => [self::SCENARIO_CHANGE_PASSWORD]],

            [["email"], "required",
                "on" => [self::SCENARIO_EDIT]],

            [
                ["rememberMe"],
                "required",
                "requiredValue" => true,
                "message" => "You must agree to the terms of service.",
                "on" => [self::SCENARIO_CREATE]
            ],

            [["password"], "validatePassword",
                "on" => [self::SCENARIO_LOGIN]],

            [["name", "login"], HtmlTagValidator::class,
                "on" => [self::SCENARIO_CREATE]],

            [['name'], 'string', 'length' => [4, 100],
                'on' => [self::SCENARIO_CREATE]],

            [['email'], 'string', 'max' => 100,
                'on' => [self::SCENARIO_CREATE, self::SCENARIO_EDIT]],

            [['login'], 'string', 'length' => [4, 45],
                'on' => [self::SCENARIO_CREATE]],

            [['name'], 'match', 'pattern' => '/^[a-z\'\‘A-Zа-яА-Я\s]+$/u',
                'on' => [self::SCENARIO_CREATE]],

            [['status'], 'in', 'range' => array_keys(self::getStatuses()),
                'on' => [self::SCENARIO_CREATE]],


            [['confirm_password'], 'compare', 'compareAttribute' => 'password',
                "on" => [self::SCENARIO_CREATE]],

            [['confirm_password'], 'compare', 'compareAttribute' => 'password',
                "on" => [self::SCENARIO_CHANGE_PASSWORD]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => t('ID'),
            'name' => t('Ф.И.О.'),
            'status' => t('Статус'),
            'phone' => t('Телефон'),
            'rememberMe' => t('Remember Me'),
            'password' => t('Пароль'),
            'created_date' => t("Дата создания"),
            'updated_at' => t("Дата редактирования"),
            "current_password" => t("Введите пароль от кабинета"),
            "new_password" => t("Новый пароль"),
            "confirm_password" => t("Повторите новый пароль"),
        ];
    }

    /**
     * @param $attribute
     */
    public function validateCurrentPassword($attribute)
    {
        if (!Yii::$app->security->validatePassword($this->current_password, $this->password)) {
            $this->addError($attribute, t("Пароль неверный."));
            return;
        }
    }


    /**
     * @param int|string $id
     * @return \yii\db\ActiveRecord|IdentityInterface|null
     * @throws \yii\web\NotFoundHttpException
     */
    public static function findIdentity($id)
    {
        /** @var self $user */
        $user = self::find()
            ->where(['id' => $id])
            ->one();

        if (!$user || !$user->isActive) {
            return null;
        }

        return $user;
    }


    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }


    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * @param string $authKey
     * @return bool|null
     */
    public function validateAuthKey($authKey)
    {
        return null;
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function login()
    {
        $user = $this->loadUser();

        return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
    }

    /**
     * @inheritDoc
     */
    private function loadUser()
    {
        if (!$this->_user) {
            $this->_user = self::find()->where(["email" => $this->email])->one();
            if ($this->_user && !$this->_user->isActive) {
                $this->_user = null;
            }
        }

        return $this->_user;
    }


    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return static::traitGetStatuses();
    }

    /**
     * @param $attribute
     */
    public function validatePassword($attribute)
    {
        $this->loadUser();

        $validated = false;
        if ($this->_user) {
            if (Yii::$app->security->validatePassword($this->password, $this->_user->password)) {
                $validated = true;
            }
        }

        if (!$validated) {
            $this->addError($attribute, t("Неверный email или пароль."));
        }
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
    }

    /**
     * @return string
     */
    public static function cacheTags()
    {
        return 'users';
    }

}
