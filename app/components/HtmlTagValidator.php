<?php

namespace app\components;

use yii\validators\Validator;

/**
 *
 */
class HtmlTagValidator extends Validator
{
    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $v = new SpaceTrimValidator();
        $v->validateAttribute($model, $attribute);

        $string = $model->{$attribute};

        if($string!='' && preg_match("/<[^<]+>/",$string,$m) != 0){
            $model->addError($attribute,t('It is forbidden to use HTML tags'));
        }
    }
}







































































