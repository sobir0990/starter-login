<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 11/13/18
 * Time: 3:03 PM
 */

namespace app\components;


/**
 * Class SerialColumn
 * @package app\components
 */
class SerialColumn extends \kartik\grid\SerialColumn
{
    /**
     * # o'rniga № yozildi
     */
    public $header = '№';

    /**
     * serial ustunlar eni 1% qilindi sababi ortiqch uzunlikda edi.
     */
    public $headerOptions =  ['style' => 'width:1%'];

    /**
     * header sozlamalari.
     */
    public $contentOptions =  ['class' => 'text-center'];
}