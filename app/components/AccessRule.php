<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 22/11/19 20:06
 */

namespace app\components;

/**
 * Class AccessRule
 * @package app\components
 */
class AccessRule extends \yii\filters\AccessRule
{
    /**
     * @param \yii\web\User $user
     * @return bool
     */
    protected function matchRole($user)
    {
        if (empty($this->roles)) {
            return false;
        }

        foreach ($this->roles as $role) {
            if ($role === '?' && $user->isGuest) {
                return true;
            }

            if ($role === '@' && !$user->isGuest) {
                return true;
            }

            if (!$user->isGuest) {
                    return true;
            }
        }
        return false;
    }
}