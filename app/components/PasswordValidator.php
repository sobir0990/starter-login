<?php
/**
 * Created by PhpStorm.
 * User: Jasur Rozmetov
 * Date: 10.12.18
 * Time: 11:09
 */

namespace app\components;

use yii\validators\Validator;

/**
 * Class PasswordValidator
 * @package app\components
 */
class PasswordValidator extends Validator
{
    public int $min = 6;

    public int $max = 20;

    /**
     * @param mixed $value
     * @return array|string|null
     */
    protected function validateValue($value)
    {
//        $hasLetters = preg_match("@[A-Za-z]+@", $value);
        $hasNumbers = preg_match("@[0-9]+@", $value);
        $hasChar = preg_match("@[^A-Za-z0-9]+@", $value);
        $hasSpace = preg_match("@[\s]+@", $value);

        if ($hasSpace) {
            return [t('The password must not contain space'), []];
        }

        if (
//            !$hasLetters ||
            !$hasNumbers ||
            !$hasChar
        ) {
//            return [t('The password must contain at least one latin letter, number and symbols.'), []];
            return [t('The password must contain at least one number and symbols.'), []];
        }

        return '';

    }
}