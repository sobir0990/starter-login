<?php

namespace app\components;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\helpers\IpHelper;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * Class User
 * @package app\components
 */
class User extends \yii\web\User
{
    const KEY_LOGIN_IP = "__login_ip__";
    const API_TOKEN = "cHJvdGV6LmlqdGltb2l5LXhpem1hdC51eg==";

    public function loginByAccessToken($token, $type = null)
    {
        return $token === self::API_TOKEN?true:null;
    }


}