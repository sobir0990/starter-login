<?php
/**
 * Created by PhpStorm.
 * User: shranet
 * Date: 11/6/18
 * Time: 2:01 PM
 */

namespace app\components;

use yii\web\NotFoundHttpException;

/**
 * Class Assert
 * @package app\components
 */
class Assert
{
    /**
     * Int ga tekshiradi. Agar int bo'lmasa 404
     *
     * @param $value
     * @throws NotFoundHttpException
     */
    public static function isInteger($value): void
    {
        if (!FilterVariable::isInteger($value))
            throw new NotFoundHttpException(t("The requested page does not exist."));
    }

    /**
     * Uint ga tekshiradi. Agar uint bo'lmasa 404
     *
     * @param $value
     * @throws NotFoundHttpException
     */
    public static function isUnsignedInteger($value): void
    {
        if (!FilterVariable::isUnsignedInteger($value))
            throw new NotFoundHttpException(t("The requested page does not exist."));
    }
}