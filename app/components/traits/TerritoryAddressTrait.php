<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 9/18/19 4:24 PM
 */

namespace app\components\traits;


use app\models\Country;
use app\models\Region;

/**
 * @property Region $region
 * @property Country $country
 *
 * Trait RegionAddressTrait
 * @package app\components\traits
 */
trait TerritoryAddressTrait
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion(): \yii\db\ActiveQuery
    {
        return $this->hasOne(Region::class, ["id" => "region_id"]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry(): \yii\db\ActiveQuery
    {
        return $this->hasOne(Country::class, ["id" => "country_id"]);
    }

    /**
     * @return string
     */
    public function getTerritoryName()
    {
        $regions = [
            $this->region->name??null,
            $this->country->name??null,
        ];
        $regions = array_filter($regions);
        return implode(', ',$regions);
    }


}