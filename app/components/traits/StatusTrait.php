<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 9/6/19 5:21 PM
 */

namespace app\components\traits;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @property $statusText
 * @property $statusLabel
 *
 * Trait StatusTrait
 * @package app\components\traits
 */
trait StatusTrait
{
    /**
     * @return mixed
     */
    public function getStatusText()
    {
        return ArrayHelper::getValue(static::getStatuses(), $this->status);
    }

    /**
     * Statusni lable bn qaytaradi
     *
     * @param $status
     * @return string
     */
    public function getStatusLabel(): string
    {
        $color = $this->status === self::STATUS_ACTIVE ? 'success' : 'danger';
        return Html::tag('span', $this->statusText,['class'=>"badge badge-$color"]);
    }

    /**
     * Statuslar ro'yhatini qaytaradi
     *
     * @return array
     */
    public static function getStatuses(): array
    {
        return [
            self::STATUS_ACTIVE => t('Активный'),
            self::STATUS_INACTIVE => t('Неактивный'),
        ];
    }

}