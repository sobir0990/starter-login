<?php
/**
 * Created by PhpStorm.
 * User: shranet
 * Date: 4/22/2019
 * Time: 5:32 PM
 */

namespace app\components\traits;


use app\components\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

trait ActiveRecordValidatorTrait
{
    /**
     * @return bool
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     */
    public function runValidate()
    {
        $args = func_get_args();
        array_unshift($args, $this);
        return call_user_func_array([static::class, 'validateFull'], $args);
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     */
    public static function validateFull()
    {
        $args = func_get_args();
        call_user_func_array([static::class, 'validateAjax'], $args);
        return call_user_func_array([static::class, 'validatePost'], $args);
    }

    /**
     * Agar kelayotgan request AJAX bo'lsa,
     * ushbu modelga yuklab validatsiyadan o'tkazadi va
     * clientga jo'natadi.
     *
     * Qo'llanishi:
     * $model->ajaxValidation()
     * @param array $list
     * @throws NotFoundHttpException Agar ma'lumotlar yuklanishda xatolik yuz bersa
     * @throws \yii\base\ExitException
     */
    public static function validateAjax()
    {
        if (!Yii::$app->request->isPost)
            return;

        if (!Yii::$app->request->isAjax)
            return;

        //Bu, PJAX formalarda uchun kerak
        //shu holatda, postValidation ishlaydi
        if (Yii::$app->request->isPjax)
            return;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $models = func_get_args();
        foreach($models as $model) {
            if (is_callable($model)) {
                continue;
            } elseif (is_array($model)) {
                if (!self::loadMultiple($model, Yii::$app->request->post()))
                    throw new NotFoundHttpException();
            } else {
                /** @var $model  */
                if (!$model->load(Yii::$app->request->post()))
                    throw new NotFoundHttpException();
            }
        }

        $result = [];
        foreach($models as $model) {
            if (is_callable($model)) {
                $result = array_merge(call_user_func($model), $result);
            } elseif (is_array($model)) {
                $result = array_merge(ActiveForm::validateMultiple($model), $result);
            } else {
                $result = array_merge(ActiveForm::validate($model), $result);
            }
        }

        Yii::$app->response->data = $result;
        Yii::$app->end();
    }

    /**
     * Agar kelayotgan request POST bo'lsa
     * ushbu modelga yuklab validatsiyadan o'tkazadi
     * Agar hammasi to'g'ri bo'lsa TRUE aks holda FALSE qaytaradi.
     *
     * @return bool
     * @throws NotFoundHttpException
     */
    public static function validatePost()
    {
        if (!Yii::$app->request->isPost)
            return false;

        $models = func_get_args();

        foreach($models as $model) {
            if (is_callable($model)) {
                continue;
            } elseif (is_array($model)) {
                if (!self::loadMultiple($model, Yii::$app->request->post()))
                    throw new NotFoundHttpException();
            } else {
                /** @var $model ActiveRecord */
                if (!$model->load(Yii::$app->request->post()))
                    throw new NotFoundHttpException();
            }
        }

        foreach($models as $model) {
            if (is_callable($model)) {
                if (!empty(call_user_func($model))) {
                    return false;
                }
            } elseif (is_array($model)) {
                if (!Model::validateMultiple($model))
                    return false;
            } else {
                if (!$model->validate())
                    return false;
            }
        }

        return true;
    }
}