<?php
/**
 * Created by PhpStorm.
 * User: shranet
 * Date: 11/9/18
 * Time: 1:52 PM
 */

namespace app\components;

use yii\base\Behavior;
use yii\web\Application;
use Yii;

/**
 * Class ApplicationInitBehavior
 * @package app\components
 */
class ApplicationInitBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            Application::EVENT_BEFORE_ACTION => 'beforeAction'
        ];
    }

    /**
     * Foydalanuvchini rolini aniqlab oladi va u bo'yicha tekshirib
     * joriy url ga kirish huquqi bor yoki yo'qligi aniqlanadi
     *
     * @return |null
     **/
    public function beforeAction(): void
    {
        if (!Yii::$app->user->isGuest and property_exists(Yii::$app->controller, 'userModel')) {
            /** @var \app\models\User $user */
            $user = Yii::$app->user->identity;

            if ($user !== null) {
                /** @var Controller $controller */
                Yii::$app->controller->userModel = $user;
            }
        }
    }

}