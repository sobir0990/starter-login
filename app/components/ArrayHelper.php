<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 11/27/18
 * Time: 10:58 AM
 */

namespace yii\helpers;

use Yii;

/**
 * Class ArrayHelper
 * @package yii\helpers
 */
class ArrayHelper extends BaseArrayHelper
{
    /**
     * Berilgan massivni alifbo bo'yicha saralab, berilgan $key ni eng yuqoriga chiqarib qo'yadi
     *
     * @param array $array
     * @param int $key
     * @return array
     */
    public static function reorderArray(array $array, int $key): array
    {
        if (array_key_exists($key, $array)) {
            if (is_array($array[$key])) {
                asort($array);
            }
            $element[$key] = $array[$key];
            $array = $element + $array;
        }
        return $array;
    }


    /**
     * Assoc Massivdan 2 - darajali berilgan $key bo'yicha o'chirib qaytaradi
     * agar $key massiv ko'rinishda berilsa, berilgan keylarni o'chiradi
     *
     * @param $array
     * @param $key
     * @return bool
     */
    public static function removeAssoc(&$array, $key): bool
    {
        if (is_array($array)) {
            foreach ($array as $k => $value) {
                if (is_string($key)) {
                    if (isset($value[$key]) || array_key_exists($key, $value)) {
                        unset($array[$k][$key]);
                    }
                } elseif (is_array($key)) {
                    foreach ($key as $item) {
                        if (isset($value[$item]) || array_key_exists($item, $value)) {
                            unset($array[$k][$item]);
                        }
                    }
                }

            }
            return true;
        }
        return false;
    }

    /**
     * @param $array
     * @param array $keys
     * @param bool $assoc
     * @return bool
     */
    public static function removeSomeKeys(&$array, array $keys, $assoc = false): bool
    {
        if (is_array($array)) {
            foreach ($array as $k => $value) {
                if ($assoc) {
                    if (is_array($value)) {
                        foreach ($value as $i => $item) {
                            if (in_array($k, $keys)) {
                                unset($array[$k][$i]);
                            }
                        }
                    }
                } else {
                    if (in_array($k, $keys)) {
                        unset($array[$k]);
                    }
                }
            }
            return true;
        }
        return false;
    }


    /**
     * Assoc massivdan berilgan ustun $column qiymati bo'yicha sanab beradi va berilgan $value lar sonini qaytaradi
     *
     * @param array $array
     * @param $column
     * @param $value
     * @return int
     */
    public static function countByValue(array $array, $column, $value): int
    {
        if (is_array($array)) {
            $result = array_count_values(array_column($array, $column));
            return isset($result[$value]) ? $result[$value] : 0;
        }
        return 0;
    }

    /**
     * Assoc massivdan  berilgan $condition_column ustunni qiymati $condition_column_value bn moslarini summasini hisoblaydi
     *
     * @param array $array
     * @param $sum_column
     * @param $condition_column
     * @param $condition_column_value
     * @return int
     */
    public static function sumByColumnValue(array $array, $sum_column, $condition_column, $condition_column_value): int
    {
        $sum = 0;
        foreach ($array as $key => $item) {
            if (isset($item[$condition_column]) and $item[$condition_column] == $condition_column_value) {
                $sum += $item[$sum_column];
            }
        }
        return (int)$sum;
    }

    /**
     *  Assoc massivdan  berilgan $condition_column ustunni qiymati $condition_column_value bn mosini
     *  tekshirib teng bo'lsa $column ustundagi qiymatni qaytaradi
     *
     * @param array $array
     * @param $column
     * @param $condition_column
     * @param $condition_column_value
     * @return int|mixed
     */
    public static function getValueByColumnValue(array $array, $column, $condition_column, $condition_column_value)
    {
        $value = null;
        foreach ($array as $key => $item) {
            if (is_array($item) and array_key_exists($condition_column,$item) and $item[$condition_column] == $condition_column_value) {
                $value = $item[$column];
                break;
            }
        }
        return $value;
    }

    /**
     * @param array $array
     * @param $condition_column
     * @param $condition_column_value
     * @param bool $keek_key
     * @return array
     */
    public static function getArrayByColumnValue(array $array, $condition_column, $condition_column_value,$keek_key=false): array
    {
        $value = [];
        foreach ($array as $key => $item) {
            if (is_array($item) and array_key_exists($condition_column,$item) and $item[$condition_column] == $condition_column_value) {
                if ($keek_key) {
                    $value[$key] = $item;
                }else{
                    $value[] = $item;
                }
            }
        }
        return $value;
    }

    /**
     * Tarjimali json yoki massiv ko'rinishdagi ma'lumotlarni joriy til bo'yicha qaytaradi
     * Agar topilmasa $defaultLanguage bo'yicha, u bo'lmasa mavjud til bo'yicha ma'lumot qaytaradi
     *
     * @param $data
     * @param null $defaultLanguage
     * @return mixed|string
     */
    public static function getValueByLanguage($data, $defaultLanguage = null)
    {
        $name = '';

        if (!is_array($data)) {
            $data = Json::decode($data);
        }

        if (is_array($data) and isset($data[Yii::$app->language]) and $data[Yii::$app->language] != '') {
            $name = $data[Yii::$app->language];
        } elseif (is_array($data) and $defaultLanguage and isset($data[$defaultLanguage])) {
            $name = $data[$defaultLanguage];
        } elseif (is_array($data)) {
            foreach ($data as $item) {
                if ($item != '') {
                    $name = $item;
                    break;
                }
            }
        }
        return $name;
    }


}


































