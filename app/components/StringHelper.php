<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 22/11/19 16:54
 */

namespace yii\helpers;

use Yii;
use yii\web\UploadedFile;

/**
 * Class StringHelper
 * @package yii\helpers
 */
class StringHelper extends BaseStringHelper
{


    const RANDOM_STRING_UPPER_CASE = 1;
    const RANDOM_STRING_LOWER_CASE = 2;
    const RANDOM_STRING_NUMBER = 4;
    const RANDOM_STRING_CHARS = 8;

    /**
     * @param int $n
     * @param false $number_only
     * @return string|null
     */
    public static function generateCode($n = 32, $number_only = false)
    {
        $type = StringHelper::RANDOM_STRING_LOWER_CASE | StringHelper::RANDOM_STRING_CHARS | StringHelper::RANDOM_STRING_NUMBER;
        if ($number_only) {
            $type = StringHelper::RANDOM_STRING_NUMBER;
        }

        return self::randomString($n, $type);
    }

}