<?php
/**
 * Created by PhpStorm.
 * User: shranet
 * Date: 6/12/2019
 * Time: 11:06 AM
 */

namespace app\components;


use yii\validators\Validator;

class SpaceTrimValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        $model->{$attribute} = $this->validateValue($model->{$attribute});
    }

    public function validateValue($value)
    {
        $new_line = "<!-NEW_LINE:".time()."->";
        $value = preg_replace("@(^\s+|\s+$)@u", "", $value);
        $value = str_replace("\n", $new_line, $value);
        $value = trim(preg_replace('@\s+@u', ' ', $value));
        return str_replace($new_line, "\n", $value);
    }
}