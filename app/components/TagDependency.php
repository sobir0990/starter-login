<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 19/02/20 09:52
 */

namespace app\components;

use Yii;
use yii\caching\CacheInterface;

/**
 * Class TagDependency
 * @package app\components
 */
class TagDependency extends \yii\caching\TagDependency
{
    /**
     * Invalidates all of the cached data items that are associated with any of the specified [[tags]].
     * @param CacheInterface $cache the cache component that caches the data items
     * @param string|array $tags
     */
    public static function invalidate($tags, $_ = null)
    {
        parent::invalidate(Yii::$app->cache, $tags);
    }
}