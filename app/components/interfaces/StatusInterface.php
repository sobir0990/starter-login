<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 9/6/19 5:15 PM
 */

namespace app\components\interfaces;

/**
 * Interface StatusInterface
 * @package app\components\interfaces
 */
interface StatusInterface
{
    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;

}