<?php
namespace app\components;

/**
 * Class NotFoundHttpException
 * @package app\components
 */
class NotFoundHttpException extends \yii\web\NotFoundHttpException
{
    /**
     * NotFoundHttpException constructor.
     * @param string|null $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(?string $message = null, int $code = 0, \Exception $previous = null)
    {
        $message = $message??t('No results were found for your request.');
        parent::__construct($message, $code, $previous);
    }
}