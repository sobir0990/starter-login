<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 11/11/19 1:24 PM
 */

namespace app\components;


/**
 * Class StrengthValidator
 * @package yii\helpers
 */
class StrengthValidator extends \kartik\password\StrengthValidator
{
    public $i18n = [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@app/messages',
        'forceTranslation' => true
    ];
}