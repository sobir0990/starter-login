<?php

namespace app\components;

use Yii;
use yii\web\HttpException;

/**
 * Class ErrorHandler
 * @package app\components
 */
class ErrorHandler extends \yii\web\ErrorHandler
{
    /**
     * @param \Error|\Exception $exception
     * @return array
     */
    protected function convertExceptionToArray($exception)
    {

        if (Yii::$app->controller->module->id != 'api') {
            return parent::convertExceptionToArray($exception);
        }

        $message = $exception->getMessage();

        if ($exception instanceof HttpException) {
            if ($exception->statusCode == 400 && !$message) {
                $message = t("Bad request");
            }
        }

        $data = [
            "status" => "error",
            "message" => $message
        ];

        if (YII_ENV_DEV) {
            $trace = [];
            foreach ($exception->getTrace() as $row) {
                if(isset($row['file'])){
                    $trace[] = $row['file'] . ":" . $row['line'];
                }
            }

            $data["trace"] = $trace;
        }

        return $data;
    }
}