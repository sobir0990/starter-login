<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 6/29/19
 * Time: 2:27 PM
 */

namespace app\components;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * Class Controller
 * @package app\components
 *
 * @property \yii\web\Response $ifRequestIsNotPjaxRedirect
 */
class Controller extends \yii\web\Controller
{

    /**
     * @var User $userModel
     */
    public $userModel;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->userModel = null;
        parent::init();
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'ruleConfig' => [
                    'class' => AccessRule::class
                ],
                'rules' => [
                    [
                        'actions' => ['login', 'register', 'send-mail', 'reset-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * @return \yii\web\Response
     */
    protected function referer(): \yii\web\Response
    {
        $referer = Yii::$app->request->referrer;
        if ($referer and $referer != Yii::$app->request->absoluteUrl) {
            $url = $referer;
        } else {
            $url = Url::to(['site/index'], true);
        }
        return $this->redirect($url);
    }


    /**
     * @return \yii\web\Response
     */
    public function getIfRequestIsNotPjaxRedirect()
    {
        if (!Yii::$app->request->isAjax and Yii::$app->request->isGet and
            !Yii::$app->request->isPost and !Yii::$app->request->isPjax) {
            return $this->referer();
        }
    }

    /**
     * @param null $model
     * @throws NotFoundHttpException
     */
    protected function checkExist($model = null)
    {
        /** @var $model ActiveRecord */
        if (!$model) {
            throw new NotFoundHttpException();
        }

    }
}