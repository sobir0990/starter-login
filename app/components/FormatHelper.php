<?php
namespace app\components;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use yii\faker\FixtureController;

/**
 * Class FormatHelper
 * @package app\components
 */
class FormatHelper
{
    /**
     * @param $phone
     * @param int $format
     * @return string
     */
    public static function phoneNumber($phone,$format = PhoneNumberFormat::INTERNATIONAL): string
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        $phone  = (false === strpos($phone, "+")?'+':'').$phone;

        try {
            $swissNumberProto = $phoneUtil->parse($phone);
        } catch (NumberParseException $e) {
            return $phone;
        }
        return $phoneUtil->format($swissNumberProto, $format);
    }
}