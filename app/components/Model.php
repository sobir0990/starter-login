<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 11/12/18
 * Time: 4:32 PM
 */

namespace app\components;

use Yii;
use yii\base\Model as BaseModel;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class Model
 * @package app\components
 */
class Model extends BaseModel
{

    /**
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     * @deprecated $model->runValidate ishlatilsin.
     */
    public function ajaxValidation(): void
    {
        self::validateAjax($this);
    }

    /**
     * @throws NotFoundHttpException
     * @deprecated $model->runValidate ishlatilsin.
     */
    public function postValidation(): bool
    {
        return self::validatePost($this);
    }

    /**
     * @return mixed
     */
    public function runValidate()
    {
        $args = func_get_args();
        array_unshift($args, $this);
        return call_user_func_array([static::class, 'validateFull'], $args);
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     */
    public static function validateFull(): bool
    {
        $args = func_get_args();
        call_user_func_array([static::class, 'validateAjax'], $args);
        return call_user_func_array([static::class, 'validatePost'], $args);
    }

    /**
     * Agar kelayotgan request AJAX bo'lsa,
     * ushbu modelga yuklab validatsiyadan o'tkazadi va
     * clientga jo'natadi.
     *
     * Qo'llanishi:
     * $model->ajaxValidation()
     * @param array $list
     * @throws NotFoundHttpException Agar ma'lumotlar yuklanishda xatolik yuz bersa
     * @throws \yii\base\ExitException
     */
    public static function validateAjax(): void
    {
        if (!Yii::$app->request->isPost)
            return;

        if (!Yii::$app->request->isAjax)
            return;

        //Bu, PJAX formalarda uchun kerak
        //shu holatda, postValidation ishlaydi
        if (Yii::$app->request->isPjax)
            return;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $models = func_get_args();

        foreach($models as $model) {
            if (is_array($model)) {
                if (!self::loadMultiple($model, Yii::$app->request->post()))
                    throw new NotFoundHttpException();
            } else {
                /** @var $model BaseModel */
                if (!$model->load(Yii::$app->request->post()))
                    throw new NotFoundHttpException();
            }
        }

        $result = [];
        foreach($models as $model) {
            if (is_array($model)) {
                $result = array_merge(ActiveForm::validateMultiple($model), $result);
            } else {
                $result = array_merge(ActiveForm::validate($model), $result);
            }
        }

        Yii::$app->response->data = $result;
        Yii::$app->end();
    }

    /**
     * Agar kelayotgan request POST bo'lsa
     * ushbu modelga yuklab validatsiyadan o'tkazadi
     * Agar hammasi to'g'ri bo'lsa TRUE aks holda FALSE qaytaradi.
     *
     * @return bool
     * @throws NotFoundHttpException
     */
    public static function validatePost(): bool
    {
        if (!Yii::$app->request->isPost)
            return false;

        $models = func_get_args();

        foreach($models as $model) {
            if (is_array($model)) {
                if (!self::loadMultiple($model, Yii::$app->request->post()))
                    throw new NotFoundHttpException();
            } else {
                /** @var $model Model */
                if (!$model->load(Yii::$app->request->post()))
                    throw new NotFoundHttpException();
            }
        }

        foreach($models as $model) {
            if (is_array($model)) {
                if (!Model::validateMultiple($model))
                    return false;
            } else {
                if (!$model->validate())
                    return false;
            }
        }

        return true;
    }
}