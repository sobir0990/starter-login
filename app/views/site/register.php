<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\User */

use app\components\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = t("Регистрация");
?>

<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
<div class="content-wrapper">
    <div class="content-header row"></div>
    <div class="content-body">
        <div class="auth-wrapper auth-v1 px-2">
            <div class="auth-inner py-2">
                <div class="card mb-0">
                    <div class="card-body">
                        <h4 class="card-title font-weight-medium mb-3"><?= ($this->title) ?></h4>

                        <?php $form = ActiveForm::begin([
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                            'options' => [
                                'class' => 'auth-register-form mt-2'
                            ]
                        ]); ?>

                        <?= $form->field($model, 'login'); ?>

                        <?= $form->field($model, 'email'); ?>

                        <?= $form->field($model, 'password', [
                            'options' => ['class' => 'form-group']
                        ])->passwordInput([
                            'placeholder' => $model->getAttributeLabel('password')
                        ])->label('Пароль') ?>

                        <?= $form->field($model, 'confirm_password', [
                            'options' => ['class' => 'form-group']
                        ])->passwordInput([
                            'placeholder' => $model->getAttributeLabel('confirm_password')
                        ])->label('Повторить пароль') ?>

                        <?= $form->field($model, 'rememberMe')->checkbox()->label('Я согласен на обработку персональных данных'); ?>

                        <?= Html::submitButton(t('Войти'), ['class' => 'btn-custom theme-bg w-100 mb-2 mt-2', 'name' => 'login-button']) ?>

                        <span>Ужа есть аккунт?</span> <a href="<?= Url::to('/login') ?>"
                                                         style="font-weight: normal; color: blue">Войдите с систему</a>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
