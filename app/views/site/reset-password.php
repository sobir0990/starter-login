<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\User */

use app\components\ActiveForm;
use yii\helpers\Html;

$this->title = t("изменение пароля");
?>

<div class="content-overlay"></div>
<div class="header-navbar-shadow"></div>
<div class="content-wrapper">
    <div class="content-header row"></div>
    <div class="content-body">
        <div class="auth-wrapper auth-v1 px-2">
            <div class="auth-inner py-2">
                <div class="card mb-0">
                    <div class="card-body">
                        <h4 class="card-title font-weight-medium mb-3"><?= ($this->title) ?></h4>

                        <?php $form = ActiveForm::begin([
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'options' => [
                                'class' => 'auth-restore-form mt-2'
                            ]
                        ]); ?>

                        <?= $form->field($model, 'password', [
                            'options' => ['class' => 'form-group']
                        ])->passwordInput([
                            'placeholder' => $model->getAttributeLabel('password')
                        ])->label('Пароль') ?>


                        <?= $form->field($model, 'confirm_password', [
                            'options' => ['class' => 'form-group']
                        ])->passwordInput([
                            'placeholder' => $model->getAttributeLabel('confirm_password')
                        ])->label('Повторить пароль') ?>


                        <?= Html::submitButton(t('Изменить'), ['class' => 'btn-custom theme-bg w-100 mb-2 mt-2', 'name' => 'login-button']) ?>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
