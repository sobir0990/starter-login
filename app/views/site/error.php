<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <div class="d-flex flex-column justify-content-center  align-items-center" style="min-height: 60vh">
        <img src="/img/error.gif">
        <h5 class="text-muted"><?= nl2br(($message)) ?></h5>
    </div>

</div>