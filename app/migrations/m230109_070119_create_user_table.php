<?php

use app\helpers\DateTimeHelper;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m230109_070119_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string(45)->notNull()->unique(),
            'name' => $this->string(100),
            'email' => $this->string(100)->notNull()->unique(),
            'phone' => $this->integer()->unique(),
            'status' => $this->smallInteger(),
            'password' => $this->string(255)->notNull(),
            'created_date' => $this->date()->notNull(),
            'city_id' => $this->binary(16),
            'avatar_id' => $this->binary(16),
            'self_about' => $this->string(2000),
            'account_details' => $this->json(),
        ]);

        $this->insert('{{%users}}', [
            'id' => 1,
            'login' => 'client',
            'status' => 1,
            'name' => 'username',
            'email' => 'client@example.com',
            'password' => \Yii::$app->security->generatePasswordHash('1234567'),
            'created_date' => date('Y-m-d'),
        ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
